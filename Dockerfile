FROM tomcat:9-jdk17

COPY ./src/ $CATALINA_HOME/lib/

EXPOSE 8080
EXPOSE 9876

ARG UID=1000
ARG GID=1000
ARG FROST_URL=https://repo1.maven.org/maven2/de/fraunhofer/iosb/ilt/FROST-Server/FROST-Server.HTTP/2.1.2/FROST-Server.HTTP-2.1.2.war


RUN mkdir -p $CATALINA_HOME/conf/Catalina/localhost \
    && apt-get update && apt-get install wget \
    && apt-get clean \
    && addgroup --system --gid ${GID} tomcat \
    && adduser --system --uid ${UID} --gid ${GID} tomcat \
    && mkdir -p $CATALINA_HOME/conf/Catalina/localhost \
    && mkdir -p $CATALINA_HOME/webapps/ROOT \
    && chown -R tomcat:tomcat $CATALINA_HOME \
    && mkdir -p /share \
    && wget -O /share/FROST-Server.war $FROST_URL \
    && chown -R tomcat /share


COPY src/index.jsp $CATALINA_HOME/webapps/ROOT/index.jsp

USER tomcat
